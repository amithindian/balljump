import {CC, Box2D} from '../globals';

const bodyTypes = {
  static: Box2D.Dynamics.b2Body.b2_staticBody,
  dynamic: Box2D.Dynamics.b2Body.b2_dynamicBody,
  kinematic: Box2D.Dynamics.b2Body.b2_kinematicBody
};

const bodyShapes = {
  circle: Box2D.Collision.Shapes.b2CircleShape,
  box: Box2D.Collision.Shapes.b2PolygonShape
};

class PhysicsFactory {
  /**
   * Physics factory constructor
   * @constructs PhysicsFactory
   * @param {Object} config physics options
   * @return {undefined}
   */
  constructor(config) {
    this.world = new Box2D.Dynamics.b2World(config.gravity); // eslint-disable-line new-cap
    this.pmr = config.pmr;
    this.world.SetContinuousPhysics(config.continues);
  }

  /**
   * World Getter
   * @return {Object} world object
   */
  getWorld() {
    return this.world;
  }

  /**
   * World Setter
   * @param {Object} world world object to set
   * @return {undefined}
   */
  setWorld(world) {
    this.world = world;
  }

  /**
   * Returns the box2d body type object
   * @param {string} type body type string notation
   * @return {Object} box2d bodytype object
   */
  getBodyType(type = 'dynamic') {
    return bodyTypes[type];
  }

  /**
   * Creates a box2d body with given config
   * @param {Object} config body options
   * @return {Object} box2d body object
   */
  createBody(config) {
    let shape = (typeof config.shape === 'object') ?
                  config.shape : this.createShape(config.shape, config);
    let bodyDef = this.createBodyDef(config.pos.x, config.pos.y, config.angle, config.type);
    let fixtDef = this.createFixtureDef(shape, config.density, config.friction, config.restitution);
    let body = this.world.CreateBody(bodyDef);
    body.CreateFixture(fixtDef);
    return body;
  }

  /**
   * Creates a box2d body defenition object with given config
   * @param {number} positionX x-position
   * @param {number} positionY y-position
   * @param {number} angle rotation in degrees --optional
   * @param {string} type body type --optional
   * @return {Object} box2d body defenition object
   */
  createBodyDef(positionX, positionY, angle = 0, type = 'dynamic') {
    let bodyType = this.getBodyType(type);
    let bodyDef = new Box2D.Dynamics.b2BodyDef(); // eslint-disable-line new-cap

    bodyDef.type = bodyType;
    bodyDef.position.Set(positionX / this.pmr, positionY / this.pmr);
    bodyDef.angle = CC.degreesToRadians(angle);
    return bodyDef;
  }

  /**
   * Creates a box2d fixture defenition object with given config
   * @param {Object} shape body shape
   * @param {number} density body mass/density --optional
   * @param {number} friction body friction constant --optional
   * @param {number} restitution bounce factor --optional
   * @return {Object} box2d fixture defenition object
   */
  createFixtureDef(shape, density = 1, friction = 0.1, restitution = 0.5) {
    let fixDef = new Box2D.Dynamics.b2FixtureDef(); // eslint-disable-line new-cap
    fixDef.shape = shape;
    fixDef.density = density;
    fixDef.friction = friction;
    fixDef.restitution = restitution;
    return fixDef;
  }

  /**
   * Creates a box2d shape with given options
   * @param {string} type shape class
   * @param {Object} options shape config object
   * @return {Object} box2d shape object
   */
  createShape(type = 'circle', options = {}) {
    let shape = new bodyShapes[type](); // eslint-disable-line new-cap
    if (type === 'circle') {
      shape.SetRadius(options.radius / this.pmr);
    } else if (type === 'box') {
      let width = options.width / this.pmr;
      let height = options.height / this.pmr;
      shape.SetAsBox(width, height);
    }
    return shape;
  }
}

export default PhysicsFactory;
