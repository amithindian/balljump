import {CC, Box2D, Document} from './globals';
import Resources from './resource';
import PhysicsFactory from './Objects/PhysicsFactory';

const HelloWorldLayer = CC.Layer.extend({
  balls: [],
  physicsFactory: null,
  PMR: 100,
  debug: true,

  ctor: function () {
    this._super();
    this.initPhysics();

    let size = CC.winSize;
    let basePos = CC.p(size.width/2, size.height/2);

    // create balls
    for (let i = 100 - 1; i >= 0; i = i - 10) {
      let ball = {};
      let pos = CC.p(basePos.x + i, basePos.y - i);

      // create sprite
      ball.sprite = new CC.Sprite(Resources.ball);
      ball.sprite.attr({
        x: pos.x,
        y: pos.y
      });

      ball.physics = this.physicsFactory.createBody({
        radius: 40,
        pos: pos,
        friction: 10
      });
      this.addChild(ball.sprite, 0);
      this.balls.push(ball);
    }

    // create ground
    let ground = {};
    ground.sprite = new CC.Sprite(Resources.ground);
    ground.sprite.attr({
      x: basePos.x,
      y: 50
    });

    ground.physics = this.physicsFactory.createBody({
      type: 'static',
      shape: 'box',
      width: size.width,
      height: 25,
      pos: CC.p(basePos.x, 50),
      friction: 10
    });
    this.addChild(ground.sprite);
    this.scheduleUpdate();
    return true;
  },

  initPhysics: function () {
    this.physicsFactory = new PhysicsFactory({
      pmr: this.PMR,
      gravity: CC.p(0, -10),
      continues: true
    });
    this.world = this.physicsFactory.getWorld();

    if (this.debug) {
      // initializing debug draw
      let canvas = Document.getElementById('test');
      let gameCanvas = Document.getElementById('Cocos2dGameContainer');
      let debugDraw = new Box2D.Dynamics.b2DebugDraw(); // eslint-disable-line new-cap

      canvas.style.width = gameCanvas.style.width;
      canvas.style.height = gameCanvas.style.height;
      canvas.style.padding = gameCanvas.style.padding;

      // test is the id of another canvas which debugdraw works on
      debugDraw.SetSprite(canvas.getContext('2d'));
      debugDraw.SetDrawScale(this.PMR);
      debugDraw.SetFillAlpha(0.3);
      debugDraw.SetLineThickness(1.0);
      debugDraw.SetFlags(Box2D.Dynamics.b2DebugDraw.e_shapeBit |
                         Box2D.Dynamics.b2DebugDraw.e_jointBit
                        );
      this.world.SetDebugDraw(debugDraw);
    }
  },

  update: function (dt) {
    this.world.Step(dt, 8, 1);
    for (let i = this.balls.length - 1; i >= 0; i = i - 1) {
      let ball = this.balls[i];
      let pos = ball.physics.GetPosition();
      let angle = CC.radiansToDegrees(ball.physics.GetAngle());
      ball.sprite.setPosition(CC.p(pos.x * this.PMR, pos.y * this.PMR));
      ball.sprite.setRotation(angle);
    }

    if (this.debug) {
      this.world.DrawDebugData();
    }
  }
});

const HelloWorldScene = CC.Scene.extend({
  onEnter: function () {
    this._super();
    let layer = new HelloWorldLayer();
    this.addChild(layer);
  }
});

export default HelloWorldScene;
